package com.company;

import java.io.*;
import java.util.HashSet;

public class Compare {

    private BufferedReader readerFirstFile;
    private BufferedReader readerSecondFile;
    private final HashSet<Different> differentSet = new HashSet<>();

    public Compare(File fistFile, File secondFile) {
        initBufferReads(fistFile, secondFile);
    }

    private void initBufferReads(File fistFile, File secondFile) {
        try {
            readerFirstFile  = new BufferedReader(new FileReader(fistFile));
            readerSecondFile = new BufferedReader(new FileReader(secondFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void startCompareFiles() throws IOException {
        long lineNumber = 1;
        String currentLineInFirstFile;
        while ((currentLineInFirstFile = readerFirstFile.readLine()) != null) {
            String currentLineInSecondFile = readerSecondFile.readLine();
            if (!currentLineInFirstFile.equalsIgnoreCase(currentLineInSecondFile)
                    && currentLineInSecondFile != null) {
                differentSet.add(new Different(currentLineInFirstFile, currentLineInSecondFile, lineNumber));
            }
            lineNumber++;
        }
    }

    public HashSet<Different> getDifferentSet() {
        return differentSet;
    }

    public int getDifferenceCount() {
        return differentSet.size();
    }

    public class Different {
        private final String differentWordInFirstFile;
        private final String differentWordInSecondFile;
        private final long lineNumber;

        public Different(String differentWordInFirstFile, String differentWordInSecondFile, long lineNumber) {
            this.differentWordInFirstFile = differentWordInFirstFile;
            this.differentWordInSecondFile = differentWordInSecondFile;
            this.lineNumber = lineNumber;
        }

        public String getDifferentWordInFirstFile() {
            return differentWordInFirstFile;
        }

        public String getDifferentWordInSecondFile() {
            return differentWordInSecondFile;
        }

        public long getLineNumber() {
            return lineNumber;
        }
    }
}
