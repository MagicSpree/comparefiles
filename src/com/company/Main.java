package com.company;

import java.io.File;
import java.io.IOException;
import java.util.Comparator;

public class Main {

    public static void main(String[] args) {
        Compare compare = new Compare(new File("first.txt"), new File("second.txt"));
        try {
            compare.startCompareFiles();
            compare.getDifferentSet()
                    .stream()
                    .sorted(Comparator.comparing(Compare.Different::getLineNumber))
                    .forEach(Main::formAMessageStr);
            print("Количество различий: " + compare.getDifferenceCount());
        } catch (IOException e) {
            print(e.getMessage());
        }
    }

    public static void formAMessageStr(Compare.Different different)
    {
        String firstWord = different.getDifferentWordInFirstFile();
        String secondWord = different.getDifferentWordInSecondFile();
        long lineNumber = different.getLineNumber();
        String message = String.format("На %d линии в первом файле слово \"%s\"" +
                ", а во втором файле слово \"%s\"", lineNumber, firstWord, secondWord);
        print(message);
    }

    public static void print(String message){
        System.out.println(message);
    }

}
